﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxMultiplicationThree
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 4, 5, 6, 7, 55, 6, 7, 8, 5, 3, 8, 7, 3, 5, 78, 6, 8, 7 ,100,7,8,9,675,4,3,1,1,1100,6,9};
            int temp = arr.Max();
            Dictionary<int, int> dict = new Dictionary<int, int>();
            for (int i = 1; i < arr.Length - 1; i++)
            {
                if (arr[i - 1] * arr[i] * arr[i + 1] > temp)
                {
                    dict.Add(i, arr[i - 1] * arr[i] * arr[i + 1]);

                }
            }

            foreach (var a in dict)
            {
                Console.WriteLine(a.Key + "   " + a.Value);
            };
            
            foreach(var a in dict)
            {
                if (a.Value == dict.Values.Max())
                {
                    
                    Console.WriteLine("max multiplication is {0} at positions {1},{2},{3} and values {4},{5} and {6}", dict.Values.Max(), a.Key - 1, a.Key , a.Key+1, arr[a.Key - 1], arr[a.Key], arr[a.Key+1]);
                }
            }
            
            Console.Read();
        }
    }
}
