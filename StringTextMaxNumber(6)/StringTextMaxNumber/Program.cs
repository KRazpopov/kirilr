﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringTextMaxNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] angabe = { "24", "18", "302", "189", "97" };

            Array.Sort(angabe);
            Array.Reverse(angabe);
            string conc = "";
            foreach (var a in angabe)
            {
                conc += a;
            }
            Console.WriteLine("max possible integer,or BigInteger is: "+conc);
            Console.Read();
        }
    }
}
