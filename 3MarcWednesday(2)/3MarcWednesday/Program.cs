﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3MarcWednesday
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Please write down a date that you consider as start date and date that is interesting for us : in format dd-mm-yyyy, for example 1-1-1900");

            string start = Console.ReadLine();
            Console.WriteLine("Please write down a date that you consider as END date in format dd-mm-yyyy, for example 1-1-1900");
            string end = Console.ReadLine();
            Console.WriteLine("Please insert day of week that program should search for");
            string dayOfWeek = Console.ReadLine();
            DayOfWeek TheDay = DayOfWeek.Monday;
            DayOfWeek[] Arr = { DayOfWeek.Monday,DayOfWeek.Tuesday,DayOfWeek.Wednesday,DayOfWeek.Thursday,DayOfWeek.Friday,DayOfWeek.Saturday,DayOfWeek.Sunday };
            for(int i = 0; i < Arr.Length; i++)
            {
                if (dayOfWeek.Equals(Arr[i].ToString()))
                {
                    TheDay = Arr[i];
                    break;
                }
            }
            DateTime Start = Convert.ToDateTime(start);
            
            DateTime End = Convert.ToDateTime(end);
            int counter = 0;
            
            while (Start <= End)
            {
                DayOfWeek temp = Start.DayOfWeek;
                if (temp == TheDay)
                    counter++;
                Start=Start.AddYears(1);
            }

            ///Console.WriteLine(dayOfWeek + "s" + Start + counter);
            Console.WriteLine("times when on the same day of the month it has been " + dayOfWeek + " in the time between " + start + " and " + end + " are " + counter);
            Console.Read();

        }
    }
}
