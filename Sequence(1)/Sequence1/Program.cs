﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckSequence
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] test = { 1, 2, 4, 3, 5, 7, 6,8,9,12,11,10 };

            bool CheckSequenceOfIntArray(int[] a)
            {
                int length = a.Length;
                int counter = 0;

                Array.Sort(a);
                int initial = a[0];
                for (int i = 0; i < length; i++)
                {
                    if (a[i] == i + initial)
                    {
                        counter++;
                    };
                };
                if (counter == length)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            };
            Console.WriteLine(CheckSequenceOfIntArray(test));
            Console.Read();
        }
    }
}